ARG HELM_VERSION
ARG KUBECTL_VERSION

# Use our helm-installer base-image:
FROM "registry.gitlab.com/cloudqure/sre/docker/cicd-helm-installer/releases/${HELM_VERSION}-kube-${KUBECTL_VERSION}"

# https://github.com/sgerrand/alpine-pkg-glibc
ARG GLIBC_VERSION

COPY src/ build/

# Install Dependencies
RUN apk add --no-cache openssl curl tar gzip bash \
  && curl -sSL -o /etc/apk/keys/sgerrand.rsa.pub https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub \
  && curl -sSL -O https://github.com/sgerrand/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/glibc-${GLIBC_VERSION}.apk \
  && apk add glibc-${GLIBC_VERSION}.apk \
  && rm glibc-${GLIBC_VERSION}.apk

RUN ln -s /build/bin/* /usr/local/bin/
