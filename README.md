# Description
Ullamco dolor consectetur est cupidatat proident. Qui amet ea aliqua ex proident officia laboris pariatur ex. Culpa laboris est nostrud sunt nostrud minim occaecat veniam incididunt dolore non eu. Ullamco laborum irure incididunt mollit voluptate ad tempor nostrud minim dolore et sunt. Exercitation culpa exercitation cillum reprehenderit irure exercitation ullamco irure consectetur sit voluptate laborum nulla. Anim consequat amet ex commodo id laborum eu. 


# Development
Scripts in this repository follow the official GitLab's shell scripting guide and enforces `shellcheck` and `shfmt` for testing the underlying shell script implementation. 


### Pipeline Test Tools
Sunt do aliquip eu mollit officia ex enim commodo aliqua voluptate proident anim.

- **shellcheck**: A shell script static analysis tool. 
https://github.com/koalaman/shellcheck   
  > ***Note**: Rather than using a 3rd part docker image, we should build our own.*
- **shfmt**: A shell parser, formatter, and interpreter.
https://github.com/PeterDaveHello/dockerized-shfmt
  > ***Note**: Rather than using a 3rd part docker image, we should build our own.*


### Building a new image
To generate a new image you must follow the git commit guidelines below, this
will trigger a semantic version bump which will then cause a new pipeline
that will build and tag the new image

# Git Commit Guidelines
This project uses Semantic Versioning. We use commit messages to automatically determine the version bumps, so they should adhere to
the [Conventional Commits (v1.0.0-beta.2)](https://www.conventionalcommits.org/en/v1.0.0-beta.2/) convention.

## TL;DR
The semantic-release plugin works by scanning the commit history from the most recent tag
and increments the version number based on the commits.

- `BREAKING CHANGE`: Commit includes a big feature/refactor which includes a breaking change. 
  > *Will bump the MAJOR version (the leftmost number).*
  > 
  > *NOTE: Both commit subject **AND** footer must contain the phrase "BREAKING CHANGE:"*.
- `feat`: Commit includes a new feature. 
  > *Will bump the MINOR version (the middle number).*
- `fix`: Commit includes a bug fix.
  > *Will bump the PATCH version (the rightmost number).*

### Additional Commit Types:
- `refactor`: A code change that neither fixes a bug nor adds a feature.
- `test`: Adding missing tests or correcting existing tests.
- `docs`: changing the readme or adding additional documentation.
- `no-release`: a specific type which you can use in specific cases when no release is needed for your change.

  > _An optional scope _may_ be provided after a type. A scope is a phrase describing a section of the codebase enclosed in parenthesis - eg. "`fix(parser): ...`"._


## About Automatic versioning
Each push to master triggers a semantic-release CI job that determines and pushes a new version tag (if any) based on commits pushed since the lasted version.   
Please notice, that this means that if a Merge Request contains several "_feat:_"  commits, only *ONE* minor version bump will occur on the merge. 

If your Merge Request includes several commits you may prefer to ignore the prefix on each individual commit and instead add an empty commit summarizing your changes like so:

```bash
git commit --allow-empty -m '[BREAKING CHANGE|feat|fix]: <changelog summary message>'
```

## Testing Semantic-Release locally: 
If you want to see the next version that will be generated (when your PR is merged), you can simply install an run `semantic-release` locally while overriding the options defined in the `.releaserc.yml` configuration file (see step 3 in the snippet below):

**NOTE**:   
You you'll need to install both `node` and `semantic-release` if you haven't don't so already - eg. using brew:  
    
```bash 
# 1. install prerequisites locally (if not installed already): 
brew update 
brew install node 

# 2. install semantic-release (with the gitlb)
npm install -g semantic-release

# 3. check the next version 
semantic-release -d --debug --ci=false --verify-conditions --publish
```

**Example (output)**:
In this example the output shows that the next version will be `v2.0.1`: 
```bash 
...
[16:04:40] [semantic-release] › ℹ  Start step "analyzeCommits" of plugin "@semantic-release/commit-analyzer"
[16:04:40] [semantic-release] [@semantic-release/commit-analyzer] › ℹ  Analyzing commit: fix: fake patch
  semantic-release:commit-analyzer Analyzing with default rules +0ms
  semantic-release:commit-analyzer The rule { type: 'fix', release: 'patch' } match commit with release type 'patch' +0ms
[16:04:40] [semantic-release] [@semantic-release/commit-analyzer] › ℹ  The release type for the commit is patch
[16:04:40] [semantic-release] [@semantic-release/commit-analyzer] › ℹ  Analyzing commit: updated semantic-release
  semantic-release:commit-analyzer Analyzing with default rules +1ms
[16:04:40] [semantic-release] [@semantic-release/commit-analyzer] › ℹ  The commit should not trigger a release
[16:04:40] [semantic-release] [@semantic-release/commit-analyzer] › ℹ  Analyzing commit: docs: update readme
  semantic-release:commit-analyzer Analyzing with default rules +1ms
[16:04:40] [semantic-release] [@semantic-release/commit-analyzer] › ℹ  The commit should not trigger a release
[16:04:40] [semantic-release] [@semantic-release/commit-analyzer] › ℹ  Analyzing commit: docs: update readme
  semantic-release:commit-analyzer Analyzing with default rules +1ms
[16:04:40] [semantic-release] [@semantic-release/commit-analyzer] › ℹ  The commit should not trigger a release
[16:04:40] [semantic-release] [@semantic-release/commit-analyzer] › ℹ  Analysis of 4 commits complete: patch release
[16:04:40] [semantic-release] › ✔  Completed step "analyzeCommits" of plugin "@semantic-release/commit-analyzer"
[16:04:40] [semantic-release] › ℹ  The next release version is 2.0.1
[16:04:40] [semantic-release] › ℹ  Start step "generateNotes" of plugin "@semantic-release/release-notes-generator"
  semantic-release:release-notes-generator version: '2.0.1' +0ms
  semantic-release:release-notes-generator host: undefined +0ms
  semantic-release:release-notes-generator owner: 'cloudqure' +0ms
  semantic-release:release-notes-generator repository: 'sre/docker/cicd-auto-deploy' +0ms
  semantic-release:release-notes-generator previousTag: 'v2.0.0' +0ms
  semantic-release:release-notes-generator currentTag: 'v2.0.1' +0ms
  semantic-release:release-notes-generator host: 'https://gitlab.com' +0ms
  semantic-release:release-notes-generator host: 'https://gitlab.com' +84ms
  semantic-release:release-notes-generator linkReferences: undefined +1ms
  semantic-release:release-notes-generator issue: 'issues' +0ms
  semantic-release:release-notes-generator commit: 'commit' +0ms
[16:04:40] [semantic-release] › ✔  Completed step "generateNotes" of plugin "@semantic-release/release-notes-generator"
[16:04:40] [semantic-release] › ⚠  Skip v2.0.1 tag creation in dry-run mode
[16:04:40] [semantic-release] › ✔  Published release 2.0.1 on default channel
[16:04:40] [semantic-release] › ℹ  Release note for version 2.0.1:
## 2.0.1 (https://gitlab.com/cloudqure/sre/docker/cicd-auto-deploy/compare/v2.0.0...v2.0.1) (2020-02-14)

### Bug Fixes

    * fake patch (9f93707 (https://gitlab.com/cloudqure/sre/docker/cicd-auto-deploy/commit/9f9370792dedc85fd37d5529ee91151504bcb2b0))

```

## Why Use Conventional Commits?
- Automatically generating CHANGELOGs.
- Automatically determining a semantic version bump (based on the types of commits landed).
- Communicating the nature of changes to teammates, the public, and other stakeholders.
- Triggering build and publish processes.
- Making it easier for people to contribute to your projects, by allowing them to explore a more structured commit history.


